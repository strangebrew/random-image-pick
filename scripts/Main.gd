extends Control

onready var dialog = get_node("FileDialog")
onready var item_list = get_node("HSplitContainer/VBoxContainer/HBoxContainer/ItemList")
onready var image_container = get_node("HSplitContainer/TextureContainer/TextureRect")
onready var random_button = get_node("HSplitContainer/VBoxContainer/Buttons/Random")
onready var clear_button = get_node("HSplitContainer/VBoxContainer/Buttons/Clear")
onready var goto_button = get_node("HSplitContainer/VBoxContainer/Buttons/GotoFile")
onready var button_group = get_groups()[0]
onready var Easing = preload("easing.gd")

signal image_selected(index)

var selected_index = -1
var image_extensions = [ "jpg", "jpeg", "png", "gif", "bmp" ]
var image_names = []
var image_paths = []
var time = 0
var delay = 0
var shuffling = false
var startValue = 0.1
var endValue = 1.0
var change = endValue - startValue
var duration = 10.0 # seconds

func _ready():
	randomize()
	connect("image_selected", self, "_on_ItemList_item_selected")

func _process(delta):
	if shuffling == false:
		return
	if image_names.size() == 0:
		return

	var wait = Easing.Back.easeInOut(time, startValue, change, duration)
	if delay < wait:
		delay += delta
	else: if delay >= wait:
		pick_random_image()
		delay = 0

	time += delta
	if time > duration:
		time = 0
		shuffling = false
		return

func dir_contents(path):
	var dir = Directory.new()
	if dir.open(path) == OK:
		dir.list_dir_begin()
		var file_name = dir.get_next()
		var folder = dir.get_current_dir()
		while file_name != "":
			if !dir.current_is_dir():
				var file_extension = file_name.get_extension().to_lower()
				if image_extensions.find(file_extension) >= 0:
					if (image_names.find(file_name) == -1):
						image_names.append(file_name)
						image_paths.append(folder + "/" + file_name)
			file_name = dir.get_next()
		dir.list_dir_end()
	else:
		print("An error occurred when trying to access the path.")

func _on_OpenFolder_pressed():
#	dialog.set_current_dir("C:/Backup D/Art stuff/References/Posters/Game Posters")
	dialog.popup()

func _on_FileDialog_dir_selected(dir):
	dir_contents(dir)
	clear_button.disabled = false
	random_button.disabled = false
	goto_button.disabled = false
	for image in image_names:
		item_list.add_item(image)

func _on_ItemList_item_selected(index):
	var path = image_paths[index]
	var texture = ImageTexture.new()
	var err = texture.load(path)
	if err != OK:
		return
	image_container.texture = texture


func _on_Shuffle_pressed():
	shuffling = true

func pick_random_image():
	var r = rand_range(0, image_names.size())
	item_list.select(r)
	item_list.ensure_current_is_visible()		
	emit_signal("image_selected", r)

func _on_Clear_pressed():
	image_names.clear()
	image_paths.clear()
	item_list.clear()
	clear_button.disabled = true
	random_button.disabled = true
	goto_button.disabled = true

func _on_GotoFile_pressed():
	var index = item_list.get_selected_items()[0]
	var path = str("file://", image_paths[index])
	print(path)
	OS.shell_open(path)
